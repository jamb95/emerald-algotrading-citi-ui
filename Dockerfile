FROM nginxinc/nginx-unprivileged:latest

EXPOSE 8080

COPY dist/emerald-ui/* /usr/share/nginx/html/