import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { StrategyService } from 'src/app/shared/services/strategy/strategy.service';
import { Strategy } from 'src/app/shared/models/strategy';
import { StockService } from 'src/app/shared/services/stock/stock.service';
import { Stock } from 'src/app/shared/models/stock';
import { FormGroup, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';

const averagesValidation: ValidatorFn = (fg: FormGroup) => {
  const start = fg.get('shortAverage').value;
  const end = fg.get('longAverage').value;
  return start !== null && end !== null && start < end
    ? null
    : { range: true };
};

@Component({
  selector: 'app-strategies',
  templateUrl: './strategies.component.html',
  styleUrls: ['./strategies.component.css']
})

export class StrategiesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'strategy', 'ticker', 'size', 'exitProfitLoss', 'profit', 'stopped', 'action'];
  strategiesTableDataSource: MatTableDataSource<Strategy>;
  allStrategies: Strategy[] = [];
  allStocks: Stock[];
  defaultStrategiesMode: boolean = true;
  areStocksCached: boolean;
  addStrategyFormStep1: FormGroup;
  addStrategyFormStep2: FormGroup;
  selectedStockIndex: number;
  strategyToBeAdded: Strategy;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    protected matBottomSheet: MatBottomSheet,
    protected strategyService: StrategyService,
    protected stockService: StockService,
    protected formBuilder: FormBuilder,
    protected matDialog: MatDialog
  ) {

  }

  get addStrategyFormStep2Controls() { return this.addStrategyFormStep2.controls; }

  ngOnInit(): void {
    this.addStrategyFormStep1 = this.formBuilder.group({
      number: ['', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]]
    });

    this.addStrategyFormStep2 = this.formBuilder.group(
      {
        strategy: ['', Validators.required],
        exitProfitLoss: ['', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]],
        shortAverage: ['', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]],
        longAverage: ['', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]]
      },
      {
        validator: [averagesValidation]
      }
    );

    this.getAllStrategies();
  }

  applyFilter(filterValue: string) {
    this.strategiesTableDataSource.filter = filterValue.trim().toLowerCase();
  }

  toggleStrategyMode(): void {
    this.defaultStrategiesMode = !this.defaultStrategiesMode;

    this.resetAddStrategyProcess();

    if (this.areStocksCached) return;

    this.stockService.findAll()
      .subscribe(
        stocksData => {
          this.allStocks = stocksData;
          this.areStocksCached = true;
        },
        err => console.log(err)
      );

  }

  chooseStockForNewStrategy(stock: Stock, index: number): void {
    if (this.selectedStockIndex === index) {
      this.resetStockSelection();
      return;
    }

    this.strategyToBeAdded = {
      stock: {
        id: stock.id,
        ticker: stock.ticker
      }
    }

    this.selectedStockIndex = index;
  }

  resetStockSelection(): void {
    this.selectedStockIndex = null;
    this.strategyToBeAdded = null;
  }

  prepareNewStrategy(): void {
    this.strategyToBeAdded.size = this.addStrategyFormStep1.get('number').value;
    this.strategyToBeAdded.exitProfitLoss = this.addStrategyFormStep2.get('exitProfitLoss').value;
    this.strategyToBeAdded.shortAvg = this.addStrategyFormStep2.get('shortAverage').value;
    this.strategyToBeAdded.longAvg = this.addStrategyFormStep2.get('longAverage').value;
  }

  createNewStrategy(): void {
    this.strategyService.createTwoMovingAveragesStrategy(this.strategyToBeAdded)
      .subscribe(
        addedStrategyEntity => {
          addedStrategyEntity.stopped = addedStrategyEntity.stopped ? 'Yes' : 'No';
          addedStrategyEntity.stockNameFlat = addedStrategyEntity.stock.ticker;
          addedStrategyEntity.strategyName = "Two Moving Averages";
          this.allStrategies.push(addedStrategyEntity);
          this.defaultStrategiesMode = true;
        },
        err => console.log(err)
      );
  }

  resetAddStrategyProcess(): void {
    this.resetStockSelection();
    this.addStrategyFormStep1.reset();
    this.addStrategyFormStep2.reset();
  }

  displayRemoveStrategyDialog(strategyToBeRemoved: Strategy): void {
    const dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: { title: 'Remove Strategy', text: 'Are you sure you want to remove this strategy?' }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result !== 'yes') return;

        this.strategyService.deleteTwoMovingAveragesStrategy(strategyToBeRemoved.id)
          .subscribe(
            onDeleteStrategySuccess => {
              this.getAllStrategies();
            },
            err => console.log(err)
          );

      });

  }


  private async getAllStrategies() {
    await this.strategyService.findAllTwoMovingAveragesStrategies().toPromise()
      .then(
        strategiesData => {
          this.allStrategies = [...this.allStrategies, ...strategiesData];
        },
        err => console.log(err)
      );

    this.allStrategies.map(strategy => {
      strategy.stopped = strategy.stopped ? 'Yes' : 'No';
      strategy.stockNameFlat = strategy.stock.ticker;
      strategy.strategyName = "Two Moving Averages";
    });

    this.strategiesTableDataSource = new MatTableDataSource(this.allStrategies);

    this.strategiesTableDataSource.sort = this.sort;

  }

}

@Component({
  selector: 'bottom-sheet-overview-example-sheet',
  template: `
    <p> Lorem </p>
  `,
})

export class StrategiesBottomSheetComponent {
  constructor(private _bottomSheetRef: MatBottomSheetRef<StrategiesBottomSheetComponent>) { }

}