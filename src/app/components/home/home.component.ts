import { Component, OnInit } from '@angular/core';
import { StockService } from 'src/app/shared/services/stock/stock.service';
import { Stock } from 'src/app/shared/models/stock';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { PopupService } from 'src/app/shared/services/popup/popup.service';
import { StrategyService } from 'src/app/shared/services/strategy/strategy.service';
import { Strategy } from 'src/app/shared/models/strategy';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  allStocks: Stock[];
  allStrategies: Strategy[] = [];
  numberOfActiveStrategies: number = 0;
  numberOfProfitableStrategies: number = 0;
  numberOfNonProfitableStrategies: number = 0;
  areStrategiesRunning: boolean;
  tickerToBeAdded: string;

  constructor(
    protected stockService: StockService,
    protected matDialog: MatDialog,
    protected popupService: PopupService,
    protected strategyService: StrategyService
  ) { }

  ngOnInit(): void {
    this.getAllStocks();
    this.getStrategiesInfo();
  }

  addStock(): void {
    if (!this.tickerToBeAdded) {

      return;
    }

    const newStock: Stock = {
      ticker: this.tickerToBeAdded
    }

    this.stockService.create(newStock)
      .subscribe(
        addedStockEntity => {
          this.allStocks.push(addedStockEntity);
          this.tickerToBeAdded = null;
        },
        err => console.log(err)
      );
  }

  displayDeleteStockDialog(stockToBeRemoved: Stock): void {
    const dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: { title: 'Remove Stock', text: 'Are you sure you want to remove this stock?' }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result !== 'yes') return;

        this.stockService.delete(stockToBeRemoved.id)
          .subscribe(
            onDeleteStockSuccess => this.allStocks = this.allStocks.filter(stock => stock.id !== stockToBeRemoved.id),
            err => console.log(err)
          );

      });
  }

  private getAllStocks(): void {
    this.stockService.findAll()
      .subscribe(
        stocksData => {
          this.allStocks = stocksData;
        },
        err => console.log(err)
      );
  }

  private async getStrategiesInfo() {
    await this.strategyService.findAllTwoMovingAveragesStrategies().toPromise()
      .then(
        strategiesData => {
          this.allStrategies = [...this.allStrategies, ...strategiesData];
        },
        err => console.log(err)
      );

    this.allStrategies.forEach(strategy => {
      if (!strategy.stopped) this.numberOfActiveStrategies++;

      if (strategy.profit > 0) this.numberOfProfitableStrategies++;
      else this.numberOfNonProfitableStrategies++;
    });
  }

}