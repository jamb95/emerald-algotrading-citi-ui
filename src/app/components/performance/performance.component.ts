import { Component, OnInit, OnDestroy } from '@angular/core';
import { Chart } from 'chart.js';
import { Strategy } from 'src/app/shared/models/strategy';
import { StrategyService } from 'src/app/shared/services/strategy/strategy.service';
import { TradeService } from 'src/app/shared/services/trade/trade.service';
import { Trade } from 'src/app/shared/models/trade';
import { NotificationService } from 'src/app/shared/services/notification/notification.service';

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.css']
})

export class PerformanceComponent implements OnInit, OnDestroy {

  chart = [];
  selectedStrategy: Strategy;
  selectedStrategyIndex: number;
  allStrategies: Strategy[] = [];
  isCanvasSpinnerShown: boolean;
  allTrades: Trade[];
  refreshChartInterval: any;
  selectedStrategyDisplayName: string;

  constructor(
    protected strategyService: StrategyService,
    protected tradeService: TradeService,
    protected notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.getAllStrategiesAndTrades();
  }

  ngOnDestroy(): void {
    clearInterval(this.refreshChartInterval);
  }

  getStrategyChart(selectedStrategy: Strategy, index: number): void {
    clearInterval(this.refreshChartInterval);

    if (this.selectedStrategyIndex === index) {
      this.resetStrategySelection();
      return;
    }

    this.selectedStrategy = selectedStrategy;
    this.selectedStrategyIndex = index;
    this.isCanvasSpinnerShown = true;

    setTimeout(() => {
      this.isCanvasSpinnerShown = false;
    }, 1000);

    setTimeout(() => {
      this.initChart();
    }, 1500);
  }


  private async getAllStrategiesAndTrades() {
    await this.strategyService.findAllTwoMovingAveragesStrategies().toPromise()
      .then(
        strategiesData => {
          this.allStrategies.map(strategy => strategy.profit = Number(Math.round(strategy.profit) + 'e-2'));
          this.allStrategies = [...this.allStrategies, ...strategiesData];
        },
        err => console.log(err)
      );

    this.allStrategies.map(strategy => {
      strategy.stopped = strategy.stopped ? 'Yes' : 'No';
      strategy.strategyName = strategy.shortAvg ? "Two Moving Averages" : "Simple Strategy";
    });

    await this.tradeService.findAll()
      .subscribe(
        allTradesData => {
          this.allTrades = allTradesData;
        },
        err => console.log(err)
      );
  }

  private resetStrategySelection(): void {
    this.selectedStrategy = null;
    this.selectedStrategyIndex = null;
    clearInterval(this.refreshChartInterval);
  }

  private initChart(): void {

    this.selectedStrategyDisplayName = "";
    this.selectedStrategyDisplayName += this.selectedStrategy.strategyName;

    let tradesForSelectedStock: any[] = this.allTrades.filter(trade => trade.strategy.id === this.selectedStrategy.id);
    let stockOperations = tradesForSelectedStock.map(trade => trade.tradeType);
    let stockPrices = tradesForSelectedStock.map(trade => trade.price);
    let timeScale = tradesForSelectedStock.map(trade => new Date(trade.lastStateChange).toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }));

    this.chart = this.constructChart(timeScale, stockPrices, stockOperations);

    if (this.selectedStrategy.stopped === 'No') {
      this.refreshChartInterval = setInterval(() => {
        this.refreshChartPeriodically();
      }, 5000);
    }
  }

  private async refreshChartPeriodically() {
    await this.strategyService.findAllTwoMovingAveragesStrategies().toPromise()
      .then(
        strategiesData => {

          const oldSelectedStrategyCopy = this.allStrategies[this.selectedStrategyIndex];
          const message = oldSelectedStrategyCopy.strategyName + " with id " + oldSelectedStrategyCopy.id + " has completed!";

          this.selectedStrategy = strategiesData[this.selectedStrategyIndex];
          this.allStrategies[this.selectedStrategyIndex] = this.selectedStrategy;

          if (this.selectedStrategy.stopped) {
            clearInterval(this.refreshChartInterval);
            this.notificationService.addMessage(message);
          }

          this.allStrategies.map(strategy => {
            strategy.stopped = strategy.stopped ? 'Yes' : 'No';
            strategy.strategyName = strategy.shortAvg ? "Two Moving Averages" : "Simple Strategy";
          });
        },
        err => console.log(err)
      );

    await this.tradeService.findAll()
      .subscribe(
        allTradesData => {
          this.allTrades.length = 0;
          this.allTrades = [...this.allTrades, ...allTradesData];

          let tradesForSelectedStock: any[] = this.allTrades.filter(trade => trade.strategy.id === this.selectedStrategy.id);
          let stockOperations = tradesForSelectedStock.map(trade => trade.tradeType);
          let stockPrices = tradesForSelectedStock.map(trade => trade.price);
          let timeScale = tradesForSelectedStock.map(trade => new Date(trade.lastStateChange).toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }));

          this.chart = this.constructChart(timeScale, stockPrices, stockOperations);
        },
        err => console.log(err)
      );
  }

  private constructChart(timeScale: any[], stockPrices: any[], stockOperations: any[]): Chart {
    return new Chart('canvas', {
      type: 'line',
      data: {
        labels: timeScale,
        datasets: [
          {
            data: stockPrices,
            borderColor: '#3cba9f',
            backgroundColor: (context) => {
              const index = context.dataIndex;
              const setpointValue = stockOperations[index];
              return setpointValue === 'SELL' ? '#fffc3d' : '#008b17';
            },
            pointRadius: 5,
            pointHoverRadius: 7,
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [
            {
              display: true,
              ticks: {
                fontColor: '#fff'
              },
              gridLines: {
                color: "#636363"
              }
            }],
          yAxes: [{
            display: true,
            ticks: {
              fontColor: '#fff'
            },
            gridLines: {
              color: "#636363"
            }
          }],
          pointLabels: { fontSize: 18 }
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {

              const tradeOperation = stockOperations[tooltipItem.index];
              let label = data.datasets[tooltipItem.datasetIndex].label || '';

              if (label) {
                label += ': ';
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;

              const finalLabel = label + " (" + tradeOperation + ")"

              return finalLabel;
            }
          }
        }
      }
    });
  }


}
