import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/shared/services/notification/notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})

export class NotificationsComponent implements OnInit {

  allNotifications: string[] = [];

  constructor(
    protected notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.allNotifications = this.notificationService.getMessageLog();
  }

  acknowledgeNotification(index: number): void {
    this.allNotifications.splice(index, 1);
    this.notificationService.removeMessage(index);
  }

}
