export interface SidebarLink {
    displayName: string,
    targetUrl: string,
    activeLinkClass: string,
    icon: string
}