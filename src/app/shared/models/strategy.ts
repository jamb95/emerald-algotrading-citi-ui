export interface Strategy {
    id?: number,
    size?: number,
    exitProfitLoss?: number,
    currentPosition?: number,
    tradePrice?: number,
    lastTradePrice?: number,
    profit?: number,
    shortAvg?: number,
    longAvg?: number,
    stopped?: any,
    stock: {
        id: number,
        ticker: string
    },
    stockNameFlat?: string,
    strategyName?: string
}