import { Strategy } from './strategy';

export interface Trade {
    accountedFor?: boolean,
    id?: number,
    lastStateChange?: string,
    price?: number,
    size?: number,
    state?: string,
    stockTicker?: string,
    strategy?: Strategy
}