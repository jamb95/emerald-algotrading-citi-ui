import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Stock } from '../../models/stock';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class StockService {

  constructor(
    protected httpClient: HttpClient
  ) { }

  findAll(): Observable<Stock[]> {
    return this.httpClient.get<Stock[]>(environment.backgroundUrl + "api/v1/stocks");
  }

  findAllAvailableStocks(): Observable<Stock[]> {
    return this.httpClient.get<Stock[]>(environment.backgroundUrl + "api/v1/stocks/prices");
  }

  create(newStock: Stock): Observable<Stock> {
    return this.httpClient.post<Stock>(environment.backgroundUrl + "api/v1/stocks", newStock);
  }

  delete(stockId: number): Observable<Stock> {
    return this.httpClient.delete<Stock>(environment.backgroundUrl + "api/v1/stocks/" + stockId);
  }

}
