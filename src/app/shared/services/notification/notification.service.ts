import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class NotificationService {

  private messageLogSource = new Subject<string[]>();
  private messageLog: string[] = [];

  addMessage(message: string) {
    this.messageLog.push(message);
    this.messageLogSource.next(this.messageLog);
  }

  getMessages(): Observable<any> {
    return this.messageLogSource.asObservable();
  }

  removeMessage(index: number): void {
    this.messageLog.splice(index, 1);
    if (this.messageLog.length) this.messageLogSource.next(this.messageLog);
    else this.messageLogSource.next([]);
  }

  getMessageLog(): string[] {
    return this.messageLog;
  }

  constructor() { }

}
