import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Strategy } from '../../models/strategy';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StrategyService {

  constructor(
    protected httpClient: HttpClient
  ) { }


  findAllSimpleStrategies(): Observable<Strategy[]> {
    return this.httpClient.get<Strategy[]>(environment.backgroundUrl + "api/v1/strategies");
  }

  findAllTwoMovingAveragesStrategies(): Observable<Strategy[]> {
    return this.httpClient.get<Strategy[]>(environment.backgroundUrl + "api/v1/averages");
  }

  createSimpleStrategy(newStrategy: Strategy): Observable<Strategy> {
    return this.httpClient.post<Strategy>(environment.backgroundUrl + "api/v1/strategies", newStrategy);
  }

  deleteSimpleStrategy(strategyId: number): Observable<Strategy> {
    return this.httpClient.delete<Strategy>(environment.backgroundUrl + "api/v1/strategies/" + strategyId);
  }

  createTwoMovingAveragesStrategy(newStrategy: Strategy): Observable<Strategy> {
    return this.httpClient.post<Strategy>(environment.backgroundUrl + "api/v1/averages", newStrategy);
  }

  deleteTwoMovingAveragesStrategy(strategyId: number): Observable<Strategy> {
    return this.httpClient.delete<Strategy>(environment.backgroundUrl + "api/v1/averages/" + strategyId);
  }

}
