import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trade } from '../../models/trade';

@Injectable({
  providedIn: 'root'
})

export class TradeService {

  constructor(
    protected httpClient: HttpClient
  ) { }

  findAll(): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>(environment.backgroundUrl + "api/v1/trades/");
  }

  findById(id: number): Observable<Trade> {
    return this.httpClient.get<Trade>(environment.backgroundUrl + "api/v1/trades/" + id);
  }

}
