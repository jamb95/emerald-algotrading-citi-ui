import { Routes } from '@angular/router';

import { HomeComponent } from '../../components/home/home.component';
import { StrategiesComponent } from '../../components/strategies/strategies.component';
import { PerformanceComponent } from '../../components/performance/performance.component';
import { NotificationsComponent } from '../../components/notifications/notifications.component';

export const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'strategies', component: StrategiesComponent },
    { path: 'performance', component: PerformanceComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full' }
];