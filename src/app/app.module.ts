import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialWidgetsModule } from './shared/modules/material-widgets/material-widgets.module';
import { AppRoutingModule } from './shared/modules/app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { StrategiesComponent, StrategiesBottomSheetComponent } from './components/strategies/strategies.component';
import { PerformanceComponent } from './components/performance/performance.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StrategiesComponent,
    PerformanceComponent,
    NotificationsComponent,
    ConfirmationDialogComponent,
    StrategiesBottomSheetComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MaterialWidgetsModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmationDialogComponent,
    StrategiesBottomSheetComponent
  ]
})

export class AppModule { }
