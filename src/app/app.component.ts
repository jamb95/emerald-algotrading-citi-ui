import { Component, OnInit, OnDestroy } from '@angular/core';
import { SidebarLink } from './shared/models/link';
import { NotificationService } from './shared/services/notification/notification.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {

  isAlertRaised: boolean;
  subscription: Subscription;

  applicationLinks: SidebarLink[] = [
    { displayName: 'HOME', targetUrl: 'home', activeLinkClass: 'sidebar-menu-active-link', icon: 'home' },
    { displayName: 'STRATEGIES', targetUrl: 'strategies', activeLinkClass: 'sidebar-menu-active-link', icon: 'timeline' },
    { displayName: 'PERFORMANCE', targetUrl: 'performance', activeLinkClass: 'sidebar-menu-active-link', icon: 'equalizer' },
    { displayName: 'NOTIFICATIONS', targetUrl: 'notifications', activeLinkClass: 'sidebar-menu-active-link', icon: 'notification_important' }
  ];

  constructor(
    protected notificationService: NotificationService
  ) {

  }

  ngOnInit(): void {
    this.subscription = this.notificationService.getMessages().subscribe(
      res => {
        console.log(res);
        if (res.length) this.isAlertRaised = true;
        else this.isAlertRaised = false;
      },
      err => console.log(err)
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

} 
